#!/bin/sh

esxcli software vib remove --vibname=nsx-adf
esxcli software vib remove --vibname=nsx-exporter
esxcli software vib remove --vibname=nsx-aggservice
esxcli software vib remove --vibname=nsx-opsagent
esxcli software vib remove --vibname=nsx-netopa
esxcli software vib remove --vibname=nsxcli
esxcli software vib remove --vibname=nsx-cfgagent
esxcli software vib remove --vibname=nsx-vdpi
esxcli software vib remove --vibname=nsx-proxy
esxcli software vib remove --vibname=nsx-cli-libs
esxcli software vib remove --vibname=nsx-nestdb
esxcli software vib remove --vibname=nsx-nestdb-libs
esxcli software vib remove --vibname=nsx-profiling-libs
esxcli software vib remove --vibname=nsx-upm-libs
esxcli software vib remove --vibname=nsx-context-mux
esxcli software vib remove --vibname=nsx-esx-datapath
esxcli software vib remove --vibname=nsx-host
esxcli software vib remove --vibname=nsx-monitoring
esxcli software vib remove --vibname=nsx-sfhc
esxcli software vib remove --vibname=nsx-platform-client
esxcli software vib remove --vibname=nsx-mpa
esxcli software vib remove --vibname=nsx-python-gevent
esxcli software vib remove --vibname=nsx-python-greenlet
esxcli software vib remove --vibname=nsx-python-logging
esxcli software vib remove --vibname=nsx-python-protobuf
esxcli software vib remove --vibname=nsx-python-utils
esxcli software vib remove --vibname=nsx-rpc-libs
esxcli software vib remove --vibname=nsx-metrics-libs
esxcli software vib remove --vibname=nsx-common-libs
esxcli software vib remove --vibname=nsx-shared-libs
esxcli software vib list |grep nsx